package com.railal.apps.proyectofinal
//////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.BluetoothSocket
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import java.io.IOException
import java.util.*

//////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
/////BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
const val REQUEST_ENABLE_BT = 1
/////BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
class MainActivity : AppCompatActivity() {

//////CCCCCCCCCCCCCCCCCCCCCCCC
//BluetoothAdapter
    lateinit var BtAdapter: BluetoothAdapter
    var mAddressDevices: ArrayAdapter<String>? = null
    var mNameDevices: ArrayAdapter<String>? = null

    companion object {
        var m_myUUID: UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
        private var m_bluetoothSocket: BluetoothSocket? = null

        var m_isConnected: Boolean = false
        lateinit var m_address: String
    }
// ///CCCCCCCCCCCCCCCCCCCCCCCC

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //////DDDDDDDDDDDDDDDDDDD
        mAddressDevices = ArrayAdapter(this, android.R.layout.simple_list_item_1)
        mNameDevices = ArrayAdapter(this, android.R.layout.simple_list_item_1)

        val verde = findViewById<Button>(R.id.verde)
        val amarillo = findViewById<Button>(R.id.amarillo)
        val rojo = findViewById<Button>(R.id.rojo)
        val pmr = findViewById<Button>(R.id.pmr)
        val start = findViewById<Button>(R.id.start)
        val conectar = findViewById<Button>(R.id.conectar)
        val dispositivo = findViewById<Button>(R.id.dispositivo)
        val spinner = findViewById<Spinner>(R.id.spinner)


        val someActivityResultLauncher = registerForActivityResult(
            StartActivityForResult()
        ) { result ->
            if (result.resultCode == REQUEST_ENABLE_BT) {
                Log.i("MainActivity", "ACTIVIDAD REGISTRADA")
            }
        }

        //Inicializacion del bluetooth adapter
        BtAdapter = (getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter

        //Checar si esta encendido o apagado
        if (BtAdapter == null) {
            Toast.makeText(this, "Bluetooth no está disponible en este dipositivo", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, "Bluetooth está disponible en este dispositivo", Toast.LENGTH_LONG).show()
        }

            //Boton dispositivos emparejados
        dispositivo.setOnClickListener {
            if (BtAdapter.isEnabled) {
                val pairedDevices: Set<BluetoothDevice>? = BtAdapter?.bondedDevices
                mAddressDevices!!.clear()
                mNameDevices!!.clear()

                pairedDevices?.forEach { device ->
                    val deviceName = device.name
                    val deviceHardwareAddress = device.address // MAC address
                    mAddressDevices!!.add(deviceHardwareAddress)
                    //........... EN ESTE PUNTO GUARDO LOS NOMBRE A MOSTRARSE EN EL COMBO BOX
                    mNameDevices!!.add(deviceName)
                }

                //ACTUALIZO LOS DISPOSITIVOS
                spinner.setAdapter(mNameDevices)
            } else {
                val noDevices = "Ningun dispositivo pudo ser emparejado"
                mAddressDevices!!.add(noDevices)
                mNameDevices!!.add(noDevices)
                Toast.makeText(this, "Primero vincule un dispositivo bluetooth", Toast.LENGTH_LONG).show()
            }
        }

        conectar.setOnClickListener {
            try {
                if (m_bluetoothSocket == null || !m_isConnected) {

                    val IntValSpin = spinner.selectedItemPosition
                    m_address = mAddressDevices!!.getItem(IntValSpin).toString()
                    Toast.makeText(this,m_address,Toast.LENGTH_LONG).show()
                    // Cancel discovery because it otherwise slows down the connection.
                    BtAdapter?.cancelDiscovery()
                    val device: BluetoothDevice = BtAdapter.getRemoteDevice(m_address)
                    m_bluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(m_myUUID)
                    m_bluetoothSocket!!.connect()
                }

                Toast.makeText(this,"CONEXION EXITOSA",Toast.LENGTH_LONG).show()
                Log.i("MainActivity", "CONEXION EXITOSA")

            } catch (e: IOException) {
                //connectSuccess = false
                e.printStackTrace()
                Toast.makeText(this,"ERROR DE CONEXION",Toast.LENGTH_LONG).show()
                Log.i("MainActivity", "ERROR DE CONEXION")
            }
        }
        /// ENCENDER LEDS
        verde.setOnClickListener{
            sendCommand("V")
        }

        amarillo.setOnClickListener{
            sendCommand("A")
        }

        rojo.setOnClickListener{
            sendCommand("R")
        }

        /// ENCENDER INTERMITENTE AMARILLO
        pmr.setOnClickListener{
            sendCommand("P")
        }

        /// ENCENDER LEDS POR SEGUNDO
        start.setOnClickListener{
            sendCommand("S")
        }
    ///////DDDDDDDDDDDDDDDDDDDDD

    }
    ///////EEEEEEEEEEEEEEEEE
    private fun sendCommand(input: String) {
        if (m_bluetoothSocket != null) {
            try{
                m_bluetoothSocket!!.outputStream.write(input.toByteArray())
            } catch(e: IOException) {
                e.printStackTrace()
            }
        }
    }
    //////EEEEEEEEEEEEEEEEEEE


}


